const provincias = [
    'Ávila',
    'Burgos',
    'León',
    'Palencia',
    'Salamanca',
    'Segovia',
    'Soria',
    'Valladolid',
    'Zamora'
];

const urls = {
    rastreadores: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?dataset=rastreadores-covid&disjunctive.provincia=true&timezone=Europe%2FMadrid&x=fecha&sort=fecha&maxpoints=&y.rastreadores.expr=rastreadores&y.rastreadores.func=SUM&y.rastreadores.cumulative=false&y.rastreadores_minimos_recomendados.expr=rastreadores_minimos_recomendados&y.rastreadores_minimos_recomendados.func=SUM&y.rastreadores_minimos_recomendados.cumulative=false&lang=es',
    positivos14: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?maxpoints=0&x=fecha&y.positivos.expr=pcr_positivos_sintomas_14dias&y.positivos.func=SUM&y.positivos.cumulative=false&y.comunitaria.expr=sospecha_transmision_comunitaria&y.comunitaria.func=SUM&y.comunitaria.cumulative=false&y.porcentaje.expr=tasapcr_positivos_sintomasx10000_14dias&y.porcentaje.func=AVG&y.porcentaje.cumulative=false&dataset=tasa-enfermos-acumulados-por-areas-de-salud&timezone=Europe%2FMadrid&lang=es',
    hospitalario_dia: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?timezone=Europe%2FMadrid&dataset=situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon&x=fecha&maxpoints=&y.fallecidos_dia.expr=nuevos_fallecimientos&y.fallecidos_dia.func=SUM&y.fallecidos_dia.cumulative=false&y.hospitalizados_planta_dia.expr=hospitalizados_planta&y.hospitalizados_planta_dia.func=SUM&y.hospitalizados_planta_dia.cumulative=false&y.hospitalizados_uci_dia.expr=hospitalizados_uci&y.hospitalizados_uci_dia.func=SUM&y.hospitalizados_uci_dia.cumulative=false&lang=es',
    fallecidos_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?timezone=Europe%2FMadrid&dataset=situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon&x=fecha&sort=fallecimientos&maxpoints=&y.fallecimientos.expr=nuevos_fallecimientos&y.fallecimientos.func=SUM&y.fallecimientos.cumulative=false&lang=es',
    hospitalizados_planta_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?timezone=Europe%2FMadrid&dataset=situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon&x=fecha&sort=hospitalizados_planta&maxpoints=&y.hospitalizados_planta.expr=hospitalizados_planta&y.hospitalizados_planta.func=SUM&y.hospitalizados_planta.cumulative=false&lang=es&disjunctive.provincia=true',
    hospitalizados_uci_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?timezone=Europe%2FMadrid&dataset=situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon&x=fecha&sort=hospitalizados_uci&maxpoints=&y.hospitalizados_uci.expr=hospitalizados_uci&y.hospitalizados_uci.func=SUM&y.hospitalizados_uci.cumulative=false&lang=es&disjunctive.provincia=true',
    nuevos_positivos: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?disjunctive.provincia=true&timezone=Europe%2FMadrid&dataset=situacion-epidemiologica-coronavirus-en-castilla-y-leon&x=fecha.year&x=fecha.month&x=fecha.day&sort=x.fecha.year,x.fecha.month,x.fecha.day&maxpoints=&y.nuevos_positivos.expr=nuevos_positivos&y.nuevos_positivos.func=SUM&y.nuevos_positivos.cumulative=false&lang=es',
    nuevos_positivos_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?disjunctive.provincia=true&timezone=Europe%2FMadrid&dataset=situacion-epidemiologica-coronavirus-en-castilla-y-leon&x=fecha&sort=nuevos_positivos&maxpoints=&y.nuevos_positivos.expr=nuevos_positivos&y.nuevos_positivos.func=SUM&y.nuevos_positivos.cumulative=false&lang=es',
    prevalencia: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?disjunctive.provincia=true&timezone=Europe%2FMadrid&dataset=prevalencia-coronavirus&x=fecha.year&x=fecha.month&x=fecha.day&sort=x.fecha.year,x.fecha.month,x.fecha.day&maxpoints=&y.prevalencia.expr=prevalencia&y.prevalencia.func=SUM&y.prevalencia.cumulative=false&lang=es',
    prevalencia_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?disjunctive.provincia=true&timezone=Europe%2FMadrid&dataset=prevalencia-coronavirus&x=fecha&sort=prevalencia&maxpoints=&y.prevalencia.expr=prevalencia&y.prevalencia.func=SUM&y.prevalencia.cumulative=false&lang=es',
    ocupacion_camas: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?dataset=ocupacion-de-camas-en-hospitales&disjunctive.provincia=true&timezone=Europe%2FMadrid&x=fecha&sort=fecha&maxpoints=&y.camas_habilitadas_planta.expr=camas_habilitadas_planta&y.camas_habilitadas_planta.func=SUM&y.camas_habilitadas_planta.cumulative=false&y.camas_habilitadas_uci.expr=camas_habilitadas_uci&y.camas_habilitadas_uci.func=SUM&y.camas_habilitadas_uci.cumulative=false&y.camas_ocupadas_planta.expr=camas_ocupadas_planta&y.camas_ocupadas_planta.func=SUM&y.camas_ocupadas_planta.cumulative=false&y.camas_iniciales_planta.expr=camas_iniciales_planta&y.camas_iniciales_planta.func=SUM&y.camas_iniciales_planta.cumulative=false&y.camas_iniciales_uci.expr=camas_iniciales_uci&y.camas_iniciales_uci.func=SUM&y.camas_iniciales_uci.cumulative=false&y.camas_ocupadas_uci.expr=camas_ocupadas_uci&y.camas_ocupadas_uci.func=SUM&y.camas_ocupadas_uci.cumulative=false&lang=es',
    indicadores_de_riesgo: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/search/?rows=1&disjunctive.indicador=true&sort=fecha&start=0&dataset=indicadores-de-riesgo-covid-19-por-provincias&timezone=Europe%2FBerlin&lang=es&refine.indicador=Indicador%20nivel%20de%20Alerta'
};

const situacion = [
    'buena',
    'preocupate',
    'mala',
    'muy mala'
];

const nivelDeRiesgo = [
    'riesgo controlado',
    'riesgo bajo',
    'riesgo medio',
    'riesgo alto',
    'riesgo muy alto'
];

const messages = {
    level1: 'Mantenga la distancia social, mascarilla e higiene respiratoria y de manos.',
    level2: 'Extreme las medidas de precaución y prevención.',
    level3: 'Restrinja al máximo el contacto social.',
    level4: 'Existen zonas con sospecha de transmisión comunitaria según criterio de la Dirección General de Salud Pública. Si se encuentra en alguna de estas zonas permanezca en su zona de salud.',
    error: 'Lo sentimos, ha ocurrido un error.'
}

const messagesRiesgo = {
    riesgo0: '',
    riesgo1: 'Riesgo muy bajo o bajo con brotes complejos o transmisión comunitaria limitada.',
    riesgo2: 'Riesgo medio, transmisión comunitaria sostenida generalizada con presión creciente sobre el sistema sanitario.',
    riesgo3: 'Riesgo alto, transmisión comunitaria no controlada y sostenida que excede las capacidades de respuesta del sistema sanitario.',
    riesgo4: 'Riesgo muy alto o extremo, transmisión comunitaria no controlada y sostenida que excede las capacidades de respuesta del sistema sanitario, y que podrá requerir medidas excepcionales.'
}

const formatDate = date =>  {
    let month = '' + (date.getMonth() + 1);
    let day = '' + date.getDate();
    let year = date.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

const getDateObjectFromString = date =>  {
    let dateArray = date.split('-');

    return new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
}

const getDateObject = date =>  {
    if (date.month.length < 2) date.month = '0' + date.month;
    if (date.day.length < 2) date.day = '0' + date.day;

    return new Date(date.year, date.month - 1, date.day);
}

const getParam = (name, url) => {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
};

const stripTags = (value) => {
    if (!value) {
        return null;
    }
    return value.replace(/(<([^>]+)>)/gi, '');
};

const setElement = (element, text) => {
    document.querySelector(`[data-element="${element}"]`).innerText = text;
};

const setDateElement = (element, date = false) => {
    let dateElement = document.querySelector(`time[data-element="${element}"]`);

    if (! date) {
        dateElement.datetime = '';
        dateElement.innerText = '-';
        return;
    }

    dateElement.setAttribute('datetime', formatDate(date));
    dateElement.innerText = date.toLocaleDateString('es', {month: "long", day: "numeric", year: "numeric"});
};

const sumValues = (items, prop) => items.reduce( (a, b) => a + b['fields'][prop], 0);

const calcTendencia = (data, property, daysAgo = 7) => {
    const length = data.length;
    let indexDaysAgo = length - daysAgo;
    let sum = 0;
    for (indexDaysAgo; indexDaysAgo < length; indexDaysAgo++) {
        sum += data[indexDaysAgo][property];
    }

    const lastDayData = Number.parseFloat(data[length - 1][property].toFixed(2));
    const averageDays = Number.parseFloat((sum / daysAgo).toFixed(2));

    if (lastDayData > averageDays) {
        return 'creciente';
    }

    if (lastDayData < averageDays) {
        return 'decreciente';
    }

    return 'estable';
};

const ajax = url => {
    let request = new XMLHttpRequest();

    return new Promise ((resolve, reject) => {
        request.open('GET', url, true);

        request.onload = () => {
            if (request.status >= 200 && request.status < 400) {
                // Success!
                resolve(JSON.parse(request.response));
            } else {
                // We reached our target server, but it returned an error
                reject();
            }
        };

        request.onerror = () => {
            reject();
        };

        request.send();      
    });
}


const orderSections = () => {
    let main = [...document.querySelectorAll('main > section')];

    var sortByFlexOrder = (a, b) => getComputedStyle(a)['order'].localeCompare(getComputedStyle(b)['order']);

    main.sort(sortByFlexOrder);
    for (var i = 0; i < main.length; i++) {
        main[i].parentNode.appendChild(main[i]);
    }

    for (var i = 0; i < main.length; i++) {
        main[i].style.visibility = 'visible';
    }

    document.querySelector('.js-mode-status').style.visibility = 'hidden';
}

const getRastreadores = provincia => {
    const fullUrl = provincia ? 
        `${urls.rastreadores}&refine.provincia=${provincia}` : 
        urls.rastreadores;

    let rastreadores = ajax(fullUrl);

    return rastreadores.then(resp => {
        let lastIndex = resp.length - 1;

        setElement('rastreadores', resp[lastIndex].rastreadores);
        setElement('rastreadores_minimos_recomendados', resp[lastIndex].rastreadores_minimos_recomendados);
        setDateElement('rastreadores_fecha', getDateObject(resp[lastIndex].x));

        let level = referenceRastreadores(resp[lastIndex].rastreadores, resp[lastIndex].rastreadores_minimos_recomendados);

        if (level)  document.getElementById('rastreadores').classList.add(level);
    }).catch(resp => {
        setElement('rastreadores_error', messages.error);
        setElement('rastreadores', '-');
        setElement('rastreadores_minimos_recomendados', '-');
        setDateElement('rastreadores_fecha');
    });
};

const referenceRastreadores = (valor, referencia) => {
    if (isNaN(valor) || isNaN(referencia)) {
        return false;
    }
    
    if (valor < referencia) {
        return 'level4';
    }
    
    if (valor == referencia) {
        return 'level2';
    }

    return 'level1';
};

const getPositivos14 = provincia => {
    const fullUrl = provincia ? 
        `${urls.positivos14}&q=(provincia:${provincia})` : 
        `${urls.positivos14}`;

    let positivos14 = ajax(fullUrl);

    return positivos14.then(resp => {
        let lastIndex = resp.length - 1;
        let positivos14Value = resp[lastIndex].porcentaje.toFixed(2);

        let tendenciaPositivos14 = calcTendencia(resp, 'porcentaje');

        setElement('positivos14', positivos14Value);
        setDateElement('positivos14_fecha', getDateObject(resp[lastIndex].x));

        let summaryClass = referencePositivos14(positivos14Value, resp[lastIndex].comunitaria);

        if(summaryClass) {
            document.getElementById('positivos14').classList.add(summaryClass);
            document.querySelector('[data-element="positivos14"]').classList.add(summaryClass, tendenciaPositivos14);
            ( summaryClass == 'level4' && ! resp[lastIndex].comunitaria ) ?
                setElement('advice', messages.level3) :
                setElement('advice', messages[summaryClass]);
        }
    }).catch(resp => {
        setElement('positivos14_error', messages.error);
        setElement('positivos14', '-');
        setDateElement('positivos14_fecha');
    });
};

const getIndicadoresDeRiesgo = provincia => {  
    const fullUrl = provincia ? `${urls.indicadores_de_riesgo}&refine.provincia=${provincia}` : `${urls.indicadores_de_riesgo}&refine.provincia=${encodeURIComponent('Castilla y León')}`;

    let indicadoresDeRiesgo = ajax(fullUrl);

    return indicadoresDeRiesgo.then(resp => {
        let summaryClass = referenceIndicadoresDeRiesgoProvincia(resp.records);

        setElement('indicadoresDeRiesgo', nivelDeRiesgo[summaryClass.slice(-1)]);
        setDateElement('indicadoresDeRiesgo_fecha', getDateObjectFromString(resp.records[0].fields.fecha));

        setElement('riskDescription', messagesRiesgo[summaryClass]);

        if(summaryClass) {
            document.getElementById('summary').classList.add(summaryClass);

            document.querySelector('.summary > svg > title').textContent = `${provincia ? decodeURIComponent(provincia) : 'Castilla y León'} se encuentra en ${nivelDeRiesgo[summaryClass.slice(-1)]}`;
        }
    }).catch(resp => {
        setElement('indicadoresDeRiesgo_error', messages.error);
        setElement('indicadoresDeRiesgo', '-');
        setDateElement('indicadoresDeRiesgo_fecha');
    });
};

const referencePositivos14 = (positivos14, comunitaria) => {
    if (isNaN(positivos14)) {
        return false;
    }
    
    if (comunitaria > 0) {
        return 'level4';
    }
    
    if (positivos14 <= 5) {
        return 'level1';
    }
    
    if (positivos14 <= 10) {
        return 'level2';
    }

    if (positivos14 <= 25) {
        return 'level3';
    }
    
    return 'level4';
};

const referenceIndicadoresDeRiesgoProvincia = indicadoresDeRiesgo => {
    const lastIndex = indicadoresDeRiesgo.length;
    // 9 is the number of indicators
    for (let index = 0; index < 9; index++) {
        if (indicadoresDeRiesgo[index].fields.indicador == "Indicador nivel de Alerta") {
            return getClassForRiesgo(indicadoresDeRiesgo[index].fields.valor === undefined ? 0 : indicadoresDeRiesgo[index].fields.valor);
        }
    }
};

const getClassForRiesgo = value => `riesgo${value}`;

const getHospitalarioDia = provincia => {
    const fullUrl = provincia ? 
        `${urls.hospitalario_dia}&refine.provincia=${provincia}` : 
        `${urls.hospitalario_dia}`;

    const fullUrlFallecidosReference = provincia ? 
        `${urls.fallecidos_reference}&refine.provincia=${provincia}` : 
        `${urls.fallecidos_reference}`;

    const fullUrlHospitalizadosPlantaReference = provincia ? 
        `${urls.hospitalizados_planta_reference}&disjunctive.provincia=true&refine.provincia=${provincia}` : 
        `${urls.hospitalizados_planta_reference}`;
    
    const fullUrlHospitalizadosUciReference = provincia ? 
        `${urls.hospitalizados_uci_reference}&disjunctive.provincia=true&refine.provincia=${provincia}` : 
        `${urls.hospitalizados_uci_reference}`;

    let hospitalarioDia = ajax(fullUrl);

    let hospitalizadosPlantaReference = ajax(fullUrlHospitalizadosPlantaReference);
    let hospitalizadosUciReference = ajax(fullUrlHospitalizadosUciReference);
    let fallecidosReference = ajax(fullUrlFallecidosReference);

    let promiseDataDay = hospitalarioDia.then(resp => {
        let lastIndex = resp.length - 1;
        let hospitalarioDiaValue = {
            hospitalizados_planta_dia: [resp[lastIndex].hospitalizados_planta_dia, calcTendencia(resp, 'hospitalizados_planta_dia')],
            hospitalizados_uci_dia: [resp[lastIndex].hospitalizados_uci_dia, calcTendencia(resp, 'hospitalizados_uci_dia')],
            fallecidos_dia: [resp[lastIndex].fallecidos_dia, calcTendencia(resp, 'fallecidos_dia')]
        };

        setElement('hospitalizados_planta_dia', hospitalarioDiaValue.hospitalizados_planta_dia[0]);
        setElement('hospitalizados_uci_dia', hospitalarioDiaValue.hospitalizados_uci_dia[0]);
        setElement('fallecidos_dia', hospitalarioDiaValue.fallecidos_dia[0]);
        setDateElement('hospitalario_dia_fecha', getDateObject(resp[lastIndex].x));
        
        return hospitalarioDiaValue;
    }).catch(resp => {
        setElement('hospitalizados_planta_dia', '-');
        setElement('hospitalizados_uci_dia', '-');
        setElement('fallecidos_dia', '-');
        setDateElement('hospitalario_dia_fecha');
    });

    let promisePlantaRef = hospitalizadosPlantaReference.then(resp => {
        let hospitalizadosPlantaReferenceValue = resp[0].hospitalizados_planta;

        setElement('hospitalizados_planta_reference', hospitalizadosPlantaReferenceValue);
        setDateElement('hospitalizados_planta_reference_fecha', getDateObject(resp[0].x));

        return hospitalizadosPlantaReferenceValue;
    }).catch(resp => {
        setElement('hospitalizados_planta_reference', '-');
        setDateElement('hospitalizados_planta_reference_fecha');
    });

    let promiseUciRef = hospitalizadosUciReference.then(resp => {
        let hospitalizadosUciReferenceValue = resp[0].hospitalizados_uci;

        setElement('hospitalizados_uci_reference', hospitalizadosUciReferenceValue);
        setDateElement('hospitalizados_uci_reference_fecha', getDateObject(resp[0].x));

        return hospitalizadosUciReferenceValue;
    }).catch(resp => {
        setElement('hospitalizados_uci_reference', '-');
        setDateElement('hospitalizados_uci_reference_fecha');
    });

    let promiseFallecidosRef = fallecidosReference.then(resp => {
        let fallecidosReferenceValue = resp[0].fallecimientos;

        setElement('fallecidos_reference', fallecidosReferenceValue);
        setDateElement('fallecidos_reference_fecha', getDateObject(resp[0].x));

        return fallecidosReferenceValue;
    }).catch(resp => {
        setElement('fallecidos_reference', '-');
        setDateElement('fallecidos_reference_fecha');
    });

    return Promise.all([promiseDataDay, promisePlantaRef, promiseUciRef, promiseFallecidosRef]).then(v => {
        let percentagePlanta = percentage(v[0].hospitalizados_planta_dia[0], v[1]);
        let percentageUci = percentage(v[0].hospitalizados_uci_dia[0], v[2]);
        let percentageFallecidos = percentage(v[0].fallecidos_dia[0], v[3]);

        if (reference100(percentagePlanta)) document.querySelector('[data-element="hospitalizados_planta_dia"]').parentElement.classList.add(reference100(percentagePlanta), v[0].hospitalizados_planta_dia[1]);
        if (reference100(percentageUci)) document.querySelector('[data-element="hospitalizados_uci_dia"]').parentElement.classList.add(reference100(percentageUci), v[0].hospitalizados_uci_dia[1]);
        if (reference100(percentageFallecidos)) document.querySelector('[data-element="fallecidos_dia"]').parentElement.classList.add(reference100(percentageFallecidos), v[0].fallecidos_dia[1]);

        let averageClass = reference100((percentagePlanta + percentageUci + percentageFallecidos) / 3);
        if (averageClass) document.getElementById('hospitalario_dia').classList.add(averageClass);
    }).catch(resp => setElement('hospitalario_dia_error', messages.error));
};

const percentage = (value, reference) => Math.round((value / reference) * 100)

const reference100 = (value) => {
    if (isNaN(value)) {
        return false;
    }
    
    if (value > 55) {
        return 'level4';
    }
    
    if (value > 30) {
        return 'level3';
    }
    
    if (value > 5) {
        return 'level2';
    }
    
    return 'level1';
};

const reference100Camas = (value) => {
    if (isNaN(value)) {
        return false;
    }
    
    if (value > 84) {
        return 'level4';
    }
    
    if (value >= 69) {
        return 'level2';
    }
    
    return 'level1';
};

const getOcupacionCamas = provincia => {
    const fullUrl = provincia ? 
        `${urls.ocupacion_camas}&refine.provincia=${provincia}` : 
        `${urls.ocupacion_camas}`;

    let ocupacionCamas = ajax(fullUrl);

    return ocupacionCamas.then(resp => {
        let lastIndex = resp.length - 1;

        let tendenciaCamasOcupadasPlanta = calcTendencia(resp, 'camas_ocupadas_planta');
        let tendenciaCamasOcupadasUci = calcTendencia(resp, 'camas_ocupadas_uci');
        
        setElement('camas_ocupadas_planta', resp[lastIndex].camas_ocupadas_planta);
        setElement('camas_habilitadas_planta', resp[lastIndex].camas_habilitadas_planta);
        setElement('camas_iniciales_planta', resp[lastIndex].camas_iniciales_planta);
        setElement('camas_ocupadas_uci', resp[lastIndex].camas_ocupadas_uci);
        setElement('camas_habilitadas_uci', resp[lastIndex].camas_habilitadas_uci);
        setElement('camas_iniciales_uci', resp[lastIndex].camas_iniciales_uci);
        setDateElement('ocupacion_hospitalaria_fecha', getDateObject(resp[lastIndex].x));

        let percentagePlanta = percentage(resp[lastIndex].camas_ocupadas_planta, resp[lastIndex].camas_habilitadas_planta);
        let percentageUci = percentage(resp[lastIndex].camas_ocupadas_uci, resp[lastIndex].camas_habilitadas_uci);

        if (reference100Camas(percentagePlanta)) {
            let planta = document.querySelector('[data-element="camas_ocupadas_planta"]');
            planta.parentElement.classList.add(reference100Camas(percentagePlanta), tendenciaCamasOcupadasPlanta);
            planta.parentElement.nextElementSibling.nextElementSibling.value = percentagePlanta;
        }

        if (reference100Camas(percentageUci)) {
            let uci = document.querySelector('[data-element="camas_ocupadas_uci"]')
            uci.parentElement.classList.add(reference100Camas(percentageUci),tendenciaCamasOcupadasUci); 
            uci.parentElement.nextElementSibling.nextElementSibling.value = percentageUci;
        }       

        let averageClass = reference100Camas(percentageUci >= percentagePlanta ? percentageUci : (percentagePlanta + percentageUci) / 2);
        if (averageClass) document.getElementById('ocupacion_hospitalaria').classList.add(averageClass);
    }).catch(resp => {
        setElement('ocupacion_hospitalaria_error', messages.error)
        setElement('camas_ocupadas_planta', '-');
        setElement('camas_habilitadas_planta', '-');
        setElement('camas_ocupadas_uci', '-');
        setElement('camas_habilitadas_uci', '-');
        setDateElement('ocupacion_hospitalaria_fecha');
    });
};

const getNuevosPositivos = provincia => {
    const fullUrl = provincia ? 
        `${urls.nuevos_positivos}&refine.provincia=${provincia}` : 
        `${urls.nuevos_positivos}`;

    const fullUrlNuevosPositivosReference = provincia ? 
        `${urls.nuevos_positivos_reference}&refine.provincia=${provincia}` : 
        `${urls.nuevos_positivos_reference}`;

    let nuevosPositivos = ajax(fullUrl);
    let nuevosPositivosReference = ajax(fullUrlNuevosPositivosReference);

    let promiseDataDay = nuevosPositivos.then(resp => {
        let lastIndex = resp.length - 1;
        let nuevosPositivosValue = resp[lastIndex].nuevos_positivos;

        let tendenciaNuevosPositivos= calcTendencia(resp, 'nuevos_positivos');

        setElement('nuevos_positivos_dia', nuevosPositivosValue);
        setDateElement('nuevos_positivos_fecha', getDateObject(resp[lastIndex].x));
        
        return [nuevosPositivosValue, tendenciaNuevosPositivos];
    }).catch(resp => {
        setElement('nuevos_positivos_dia', '-');
        setDateElement('nuevos_positivos_fecha');
    });

    let promiseDataRef = nuevosPositivosReference.then(resp => {
        let nuevosPositivosReferenceValue = resp[0].nuevos_positivos;

        setElement('nuevos_positivos_reference', nuevosPositivosReferenceValue);
        setDateElement('nuevos_positivos_reference_fecha', getDateObject(resp[0].x));

        return nuevosPositivosReferenceValue;
    }).catch(resp => {
        setElement('nuevos_positivos_reference', '-');
        setDateElement('nuevos_positivos_reference_fecha');
    });

    return Promise.all([promiseDataDay, promiseDataRef]).then(v => {
        let percentageDay = percentage(v[0][0], v[1]);
        let levelClass = reference100(percentageDay);
        if (levelClass) {
            document.querySelector('[data-element="nuevos_positivos_dia"]').parentElement.classList.add(levelClass, v[0][1]);
            document.getElementById('nuevos_positivos').classList.add(levelClass);
        }
    }).catch(resp => setElement('nuevos_positivos_error', messages.error));
};

const getPrevalencia = provincia => {
    const fullUrl = provincia ? 
        `${urls.prevalencia}&refine.provincia=${provincia}` : 
        `${urls.prevalencia}`;

    const fullUrlPrevalenciaReference = provincia ? 
        `${urls.prevalencia_reference}&refine.provincia=${provincia}` : 
        `${urls.prevalencia_reference}`;

    let prevalencia = ajax(fullUrl);
    let prevalenciaReference = ajax(fullUrlPrevalenciaReference);

    let promiseDataDay = prevalencia.then(resp => {
        let lastIndex = resp.length - 1;
        let prevalenciaValue = resp[lastIndex].prevalencia;

        let tendenciaPrevalencia = calcTendencia(resp, 'prevalencia');

        setElement('prevalencia_dia', prevalenciaValue);
        setDateElement('prevalencia_fecha', getDateObject(resp[lastIndex].x));
        
        return [prevalenciaValue, tendenciaPrevalencia];
    }).catch(resp => {
        setElement('prevalencia_dia', '-');
        setDateElement('prevalencia_fecha');
    });

    let promiseDataRef = prevalenciaReference.then(resp => {
        let prevalenciaReferenceValue = resp[0].prevalencia;

        setElement('prevalencia_reference', prevalenciaReferenceValue);
        setDateElement('prevalencia_reference_fecha', getDateObject(resp[0].x));

        return prevalenciaReferenceValue;
    }).catch(resp => {
        setElement('prevalencia_reference', '-');
        setDateElement('prevalencia_reference_fecha');
    });

    return Promise.all([promiseDataDay, promiseDataRef]).then(v => {
        let percentageDay = percentage(v[0][0], v[1]);
        let levelClass = reference100(percentageDay);
        if (levelClass) {
            document.querySelector('[data-element="prevalencia_dia"]').parentElement.classList.add(levelClass, v[0][1]);
            document.getElementById('prevalencia').classList.add(levelClass);
        }
    }).catch(resp => setElement('prevalencia_error', messages.error));
};

const init = () => {
    const provinciaParam = stripTags(getParam('provincia'));
    const provinciaElements = document.querySelectorAll('[data-element="provincia"]');

    let provincia = provincias.includes(provinciaParam) ? provinciaParam : 'Castilla y León';

    const selects = document.querySelectorAll('select');

    for (const key in selects) {
        if (selects.hasOwnProperty(key)) {
            selects[key].value = provincia;
        }
    }

    setElement('title', `Situación simplificada COVID19 en ${provincia}`);

    for (const key in provinciaElements) {
        if (provinciaElements.hasOwnProperty(key)) {
            provinciaElements[key].innerText = provincia;
        }
    }

    provincia = (provincia === 'Castilla y León') ? false : encodeURIComponent(provincia);

    Promise.all([
        getRastreadores(provincia),
        getPositivos14(provincia),
        getHospitalarioDia(provincia),
        getOcupacionCamas(provincia),
        getNuevosPositivos(provincia),
        getPrevalencia(provincia),
        getIndicadoresDeRiesgo(provincia)
    ]).then(() => orderSections());
};

document.addEventListener('DOMContentLoaded', init);


if ('serviceWorker' in navigator) {
    window.addEventListener('load', () => {
        navigator.serviceWorker.register('/service-worker.js');
    });
}