/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/main.js":
/*!******************************!*\
  !*** ./resources/js/main.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var provincias = ['Ávila', 'Burgos', 'León', 'Palencia', 'Salamanca', 'Segovia', 'Soria', 'Valladolid', 'Zamora'];
var urls = {
  rastreadores: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?dataset=rastreadores-covid&disjunctive.provincia=true&timezone=Europe%2FMadrid&x=fecha&sort=fecha&maxpoints=&y.rastreadores.expr=rastreadores&y.rastreadores.func=SUM&y.rastreadores.cumulative=false&y.rastreadores_minimos_recomendados.expr=rastreadores_minimos_recomendados&y.rastreadores_minimos_recomendados.func=SUM&y.rastreadores_minimos_recomendados.cumulative=false&lang=es',
  positivos14: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?maxpoints=0&x=fecha&y.positivos.expr=pcr_positivos_sintomas_14dias&y.positivos.func=SUM&y.positivos.cumulative=false&y.comunitaria.expr=sospecha_transmision_comunitaria&y.comunitaria.func=SUM&y.comunitaria.cumulative=false&y.porcentaje.expr=tasapcr_positivos_sintomasx10000_14dias&y.porcentaje.func=AVG&y.porcentaje.cumulative=false&dataset=tasa-enfermos-acumulados-por-areas-de-salud&timezone=Europe%2FMadrid&lang=es',
  hospitalario_dia: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?timezone=Europe%2FMadrid&dataset=situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon&x=fecha&maxpoints=&y.fallecidos_dia.expr=nuevos_fallecimientos&y.fallecidos_dia.func=SUM&y.fallecidos_dia.cumulative=false&y.hospitalizados_planta_dia.expr=hospitalizados_planta&y.hospitalizados_planta_dia.func=SUM&y.hospitalizados_planta_dia.cumulative=false&y.hospitalizados_uci_dia.expr=hospitalizados_uci&y.hospitalizados_uci_dia.func=SUM&y.hospitalizados_uci_dia.cumulative=false&lang=es',
  fallecidos_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?timezone=Europe%2FMadrid&dataset=situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon&x=fecha&sort=fallecimientos&maxpoints=&y.fallecimientos.expr=nuevos_fallecimientos&y.fallecimientos.func=SUM&y.fallecimientos.cumulative=false&lang=es',
  hospitalizados_planta_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?timezone=Europe%2FMadrid&dataset=situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon&x=fecha&sort=hospitalizados_planta&maxpoints=&y.hospitalizados_planta.expr=hospitalizados_planta&y.hospitalizados_planta.func=SUM&y.hospitalizados_planta.cumulative=false&lang=es&disjunctive.provincia=true',
  hospitalizados_uci_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?timezone=Europe%2FMadrid&dataset=situacion-de-hospitalizados-por-coronavirus-en-castilla-y-leon&x=fecha&sort=hospitalizados_uci&maxpoints=&y.hospitalizados_uci.expr=hospitalizados_uci&y.hospitalizados_uci.func=SUM&y.hospitalizados_uci.cumulative=false&lang=es&disjunctive.provincia=true',
  nuevos_positivos: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?disjunctive.provincia=true&timezone=Europe%2FMadrid&dataset=situacion-epidemiologica-coronavirus-en-castilla-y-leon&x=fecha.year&x=fecha.month&x=fecha.day&sort=x.fecha.year,x.fecha.month,x.fecha.day&maxpoints=&y.nuevos_positivos.expr=nuevos_positivos&y.nuevos_positivos.func=SUM&y.nuevos_positivos.cumulative=false&lang=es',
  nuevos_positivos_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?disjunctive.provincia=true&timezone=Europe%2FMadrid&dataset=situacion-epidemiologica-coronavirus-en-castilla-y-leon&x=fecha&sort=nuevos_positivos&maxpoints=&y.nuevos_positivos.expr=nuevos_positivos&y.nuevos_positivos.func=SUM&y.nuevos_positivos.cumulative=false&lang=es',
  prevalencia: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?disjunctive.provincia=true&timezone=Europe%2FMadrid&dataset=prevalencia-coronavirus&x=fecha.year&x=fecha.month&x=fecha.day&sort=x.fecha.year,x.fecha.month,x.fecha.day&maxpoints=&y.prevalencia.expr=prevalencia&y.prevalencia.func=SUM&y.prevalencia.cumulative=false&lang=es',
  prevalencia_reference: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?disjunctive.provincia=true&timezone=Europe%2FMadrid&dataset=prevalencia-coronavirus&x=fecha&sort=prevalencia&maxpoints=&y.prevalencia.expr=prevalencia&y.prevalencia.func=SUM&y.prevalencia.cumulative=false&lang=es',
  ocupacion_camas: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/analyze/?dataset=ocupacion-de-camas-en-hospitales&disjunctive.provincia=true&timezone=Europe%2FMadrid&x=fecha&sort=fecha&maxpoints=&y.camas_habilitadas_planta.expr=camas_habilitadas_planta&y.camas_habilitadas_planta.func=SUM&y.camas_habilitadas_planta.cumulative=false&y.camas_habilitadas_uci.expr=camas_habilitadas_uci&y.camas_habilitadas_uci.func=SUM&y.camas_habilitadas_uci.cumulative=false&y.camas_ocupadas_planta.expr=camas_ocupadas_planta&y.camas_ocupadas_planta.func=SUM&y.camas_ocupadas_planta.cumulative=false&y.camas_iniciales_planta.expr=camas_iniciales_planta&y.camas_iniciales_planta.func=SUM&y.camas_iniciales_planta.cumulative=false&y.camas_iniciales_uci.expr=camas_iniciales_uci&y.camas_iniciales_uci.func=SUM&y.camas_iniciales_uci.cumulative=false&y.camas_ocupadas_uci.expr=camas_ocupadas_uci&y.camas_ocupadas_uci.func=SUM&y.camas_ocupadas_uci.cumulative=false&lang=es',
  indicadores_de_riesgo: 'https://analisis.datosabiertos.jcyl.es/api/records/1.0/search/?rows=1&disjunctive.indicador=true&sort=fecha&start=0&dataset=indicadores-de-riesgo-covid-19-por-provincias&timezone=Europe%2FBerlin&lang=es&refine.indicador=Indicador%20nivel%20de%20Alerta'
};
var situacion = ['buena', 'preocupate', 'mala', 'muy mala'];
var nivelDeRiesgo = ['riesgo controlado', 'riesgo bajo', 'riesgo medio', 'riesgo alto', 'riesgo muy alto'];
var messages = {
  level1: 'Mantenga la distancia social, mascarilla e higiene respiratoria y de manos.',
  level2: 'Extreme las medidas de precaución y prevención.',
  level3: 'Restrinja al máximo el contacto social.',
  level4: 'Existen zonas con sospecha de transmisión comunitaria según criterio de la Dirección General de Salud Pública. Si se encuentra en alguna de estas zonas permanezca en su zona de salud.',
  error: 'Lo sentimos, ha ocurrido un error.'
};
var messagesRiesgo = {
  riesgo0: '',
  riesgo1: 'Riesgo muy bajo o bajo con brotes complejos o transmisión comunitaria limitada.',
  riesgo2: 'Riesgo medio, transmisión comunitaria sostenida generalizada con presión creciente sobre el sistema sanitario.',
  riesgo3: 'Riesgo alto, transmisión comunitaria no controlada y sostenida que excede las capacidades de respuesta del sistema sanitario.',
  riesgo4: 'Riesgo muy alto o extremo, transmisión comunitaria no controlada y sostenida que excede las capacidades de respuesta del sistema sanitario, y que podrá requerir medidas excepcionales.'
};

var formatDate = function formatDate(date) {
  var month = '' + (date.getMonth() + 1);
  var day = '' + date.getDate();
  var year = date.getFullYear();
  if (month.length < 2) month = '0' + month;
  if (day.length < 2) day = '0' + day;
  return [year, month, day].join('-');
};

var getDateObjectFromString = function getDateObjectFromString(date) {
  var dateArray = date.split('-');
  return new Date(dateArray[0], dateArray[1] - 1, dateArray[2]);
};

var getDateObject = function getDateObject(date) {
  if (date.month.length < 2) date.month = '0' + date.month;
  if (date.day.length < 2) date.day = '0' + date.day;
  return new Date(date.year, date.month - 1, date.day);
};

var getParam = function getParam(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
};

var stripTags = function stripTags(value) {
  if (!value) {
    return null;
  }

  return value.replace(/(<([^>]+)>)/gi, '');
};

var setElement = function setElement(element, text) {
  document.querySelector("[data-element=\"".concat(element, "\"]")).innerText = text;
};

var setDateElement = function setDateElement(element) {
  var date = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
  var dateElement = document.querySelector("time[data-element=\"".concat(element, "\"]"));

  if (!date) {
    dateElement.datetime = '';
    dateElement.innerText = '-';
    return;
  }

  dateElement.setAttribute('datetime', formatDate(date));
  dateElement.innerText = date.toLocaleDateString('es', {
    month: "long",
    day: "numeric",
    year: "numeric"
  });
};

var sumValues = function sumValues(items, prop) {
  return items.reduce(function (a, b) {
    return a + b['fields'][prop];
  }, 0);
};

var calcTendencia = function calcTendencia(data, property) {
  var daysAgo = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 7;
  var length = data.length;
  var indexDaysAgo = length - daysAgo;
  var sum = 0;

  for (indexDaysAgo; indexDaysAgo < length; indexDaysAgo++) {
    sum += data[indexDaysAgo][property];
  }

  var lastDayData = Number.parseFloat(data[length - 1][property].toFixed(2));
  var averageDays = Number.parseFloat((sum / daysAgo).toFixed(2));

  if (lastDayData > averageDays) {
    return 'creciente';
  }

  if (lastDayData < averageDays) {
    return 'decreciente';
  }

  return 'estable';
};

var ajax = function ajax(url) {
  var request = new XMLHttpRequest();
  return new Promise(function (resolve, reject) {
    request.open('GET', url, true);

    request.onload = function () {
      if (request.status >= 200 && request.status < 400) {
        // Success!
        resolve(JSON.parse(request.response));
      } else {
        // We reached our target server, but it returned an error
        reject();
      }
    };

    request.onerror = function () {
      reject();
    };

    request.send();
  });
};

var orderSections = function orderSections() {
  var main = _toConsumableArray(document.querySelectorAll('main > section'));

  var sortByFlexOrder = function sortByFlexOrder(a, b) {
    return getComputedStyle(a)['order'].localeCompare(getComputedStyle(b)['order']);
  };

  main.sort(sortByFlexOrder);

  for (var i = 0; i < main.length; i++) {
    main[i].parentNode.appendChild(main[i]);
  }

  for (var i = 0; i < main.length; i++) {
    main[i].style.visibility = 'visible';
  }

  document.querySelector('.js-mode-status').style.visibility = 'hidden';
};

var getRastreadores = function getRastreadores(provincia) {
  var fullUrl = provincia ? "".concat(urls.rastreadores, "&refine.provincia=").concat(provincia) : urls.rastreadores;
  var rastreadores = ajax(fullUrl);
  return rastreadores.then(function (resp) {
    var lastIndex = resp.length - 1;
    setElement('rastreadores', resp[lastIndex].rastreadores);
    setElement('rastreadores_minimos_recomendados', resp[lastIndex].rastreadores_minimos_recomendados);
    setDateElement('rastreadores_fecha', getDateObject(resp[lastIndex].x));
    var level = referenceRastreadores(resp[lastIndex].rastreadores, resp[lastIndex].rastreadores_minimos_recomendados);
    if (level) document.getElementById('rastreadores').classList.add(level);
  })["catch"](function (resp) {
    setElement('rastreadores_error', messages.error);
    setElement('rastreadores', '-');
    setElement('rastreadores_minimos_recomendados', '-');
    setDateElement('rastreadores_fecha');
  });
};

var referenceRastreadores = function referenceRastreadores(valor, referencia) {
  if (isNaN(valor) || isNaN(referencia)) {
    return false;
  }

  if (valor < referencia) {
    return 'level4';
  }

  if (valor == referencia) {
    return 'level2';
  }

  return 'level1';
};

var getPositivos14 = function getPositivos14(provincia) {
  var fullUrl = provincia ? "".concat(urls.positivos14, "&q=(provincia:").concat(provincia, ")") : "".concat(urls.positivos14);
  var positivos14 = ajax(fullUrl);
  return positivos14.then(function (resp) {
    var lastIndex = resp.length - 1;
    var positivos14Value = resp[lastIndex].porcentaje.toFixed(2);
    var tendenciaPositivos14 = calcTendencia(resp, 'porcentaje');
    setElement('positivos14', positivos14Value);
    setDateElement('positivos14_fecha', getDateObject(resp[lastIndex].x));
    var summaryClass = referencePositivos14(positivos14Value, resp[lastIndex].comunitaria);

    if (summaryClass) {
      document.getElementById('positivos14').classList.add(summaryClass);
      document.querySelector('[data-element="positivos14"]').classList.add(summaryClass, tendenciaPositivos14);
      summaryClass == 'level4' && !resp[lastIndex].comunitaria ? setElement('advice', messages.level3) : setElement('advice', messages[summaryClass]);
    }
  })["catch"](function (resp) {
    setElement('positivos14_error', messages.error);
    setElement('positivos14', '-');
    setDateElement('positivos14_fecha');
  });
};

var getIndicadoresDeRiesgo = function getIndicadoresDeRiesgo(provincia) {
  var fullUrl = provincia ? "".concat(urls.indicadores_de_riesgo, "&refine.provincia=").concat(provincia) : "".concat(urls.indicadores_de_riesgo, "&refine.provincia=").concat(encodeURIComponent('Castilla y León'));
  var indicadoresDeRiesgo = ajax(fullUrl);
  return indicadoresDeRiesgo.then(function (resp) {
    var summaryClass = referenceIndicadoresDeRiesgoProvincia(resp.records);
    setElement('indicadoresDeRiesgo', nivelDeRiesgo[summaryClass.slice(-1)]);
    setDateElement('indicadoresDeRiesgo_fecha', getDateObjectFromString(resp.records[0].fields.fecha));
    setElement('riskDescription', messagesRiesgo[summaryClass]);

    if (summaryClass) {
      document.getElementById('summary').classList.add(summaryClass);
      document.querySelector('.summary > svg > title').textContent = "".concat(provincia ? decodeURIComponent(provincia) : 'Castilla y León', " se encuentra en ").concat(nivelDeRiesgo[summaryClass.slice(-1)]);
    }
  })["catch"](function (resp) {
    setElement('indicadoresDeRiesgo_error', messages.error);
    setElement('indicadoresDeRiesgo', '-');
    setDateElement('indicadoresDeRiesgo_fecha');
  });
};

var referencePositivos14 = function referencePositivos14(positivos14, comunitaria) {
  if (isNaN(positivos14)) {
    return false;
  }

  if (comunitaria > 0) {
    return 'level4';
  }

  if (positivos14 <= 5) {
    return 'level1';
  }

  if (positivos14 <= 10) {
    return 'level2';
  }

  if (positivos14 <= 25) {
    return 'level3';
  }

  return 'level4';
};

var referenceIndicadoresDeRiesgoProvincia = function referenceIndicadoresDeRiesgoProvincia(indicadoresDeRiesgo) {
  var lastIndex = indicadoresDeRiesgo.length; // 9 is the number of indicators

  for (var index = 0; index < 9; index++) {
    if (indicadoresDeRiesgo[index].fields.indicador == "Indicador nivel de Alerta") {
      return getClassForRiesgo(indicadoresDeRiesgo[index].fields.valor === undefined ? 0 : indicadoresDeRiesgo[index].fields.valor);
    }
  }
};

var getClassForRiesgo = function getClassForRiesgo(value) {
  return "riesgo".concat(value);
};

var getHospitalarioDia = function getHospitalarioDia(provincia) {
  var fullUrl = provincia ? "".concat(urls.hospitalario_dia, "&refine.provincia=").concat(provincia) : "".concat(urls.hospitalario_dia);
  var fullUrlFallecidosReference = provincia ? "".concat(urls.fallecidos_reference, "&refine.provincia=").concat(provincia) : "".concat(urls.fallecidos_reference);
  var fullUrlHospitalizadosPlantaReference = provincia ? "".concat(urls.hospitalizados_planta_reference, "&disjunctive.provincia=true&refine.provincia=").concat(provincia) : "".concat(urls.hospitalizados_planta_reference);
  var fullUrlHospitalizadosUciReference = provincia ? "".concat(urls.hospitalizados_uci_reference, "&disjunctive.provincia=true&refine.provincia=").concat(provincia) : "".concat(urls.hospitalizados_uci_reference);
  var hospitalarioDia = ajax(fullUrl);
  var hospitalizadosPlantaReference = ajax(fullUrlHospitalizadosPlantaReference);
  var hospitalizadosUciReference = ajax(fullUrlHospitalizadosUciReference);
  var fallecidosReference = ajax(fullUrlFallecidosReference);
  var promiseDataDay = hospitalarioDia.then(function (resp) {
    var lastIndex = resp.length - 1;
    var hospitalarioDiaValue = {
      hospitalizados_planta_dia: [resp[lastIndex].hospitalizados_planta_dia, calcTendencia(resp, 'hospitalizados_planta_dia')],
      hospitalizados_uci_dia: [resp[lastIndex].hospitalizados_uci_dia, calcTendencia(resp, 'hospitalizados_uci_dia')],
      fallecidos_dia: [resp[lastIndex].fallecidos_dia, calcTendencia(resp, 'fallecidos_dia')]
    };
    setElement('hospitalizados_planta_dia', hospitalarioDiaValue.hospitalizados_planta_dia[0]);
    setElement('hospitalizados_uci_dia', hospitalarioDiaValue.hospitalizados_uci_dia[0]);
    setElement('fallecidos_dia', hospitalarioDiaValue.fallecidos_dia[0]);
    setDateElement('hospitalario_dia_fecha', getDateObject(resp[lastIndex].x));
    return hospitalarioDiaValue;
  })["catch"](function (resp) {
    setElement('hospitalizados_planta_dia', '-');
    setElement('hospitalizados_uci_dia', '-');
    setElement('fallecidos_dia', '-');
    setDateElement('hospitalario_dia_fecha');
  });
  var promisePlantaRef = hospitalizadosPlantaReference.then(function (resp) {
    var hospitalizadosPlantaReferenceValue = resp[0].hospitalizados_planta;
    setElement('hospitalizados_planta_reference', hospitalizadosPlantaReferenceValue);
    setDateElement('hospitalizados_planta_reference_fecha', getDateObject(resp[0].x));
    return hospitalizadosPlantaReferenceValue;
  })["catch"](function (resp) {
    setElement('hospitalizados_planta_reference', '-');
    setDateElement('hospitalizados_planta_reference_fecha');
  });
  var promiseUciRef = hospitalizadosUciReference.then(function (resp) {
    var hospitalizadosUciReferenceValue = resp[0].hospitalizados_uci;
    setElement('hospitalizados_uci_reference', hospitalizadosUciReferenceValue);
    setDateElement('hospitalizados_uci_reference_fecha', getDateObject(resp[0].x));
    return hospitalizadosUciReferenceValue;
  })["catch"](function (resp) {
    setElement('hospitalizados_uci_reference', '-');
    setDateElement('hospitalizados_uci_reference_fecha');
  });
  var promiseFallecidosRef = fallecidosReference.then(function (resp) {
    var fallecidosReferenceValue = resp[0].fallecimientos;
    setElement('fallecidos_reference', fallecidosReferenceValue);
    setDateElement('fallecidos_reference_fecha', getDateObject(resp[0].x));
    return fallecidosReferenceValue;
  })["catch"](function (resp) {
    setElement('fallecidos_reference', '-');
    setDateElement('fallecidos_reference_fecha');
  });
  return Promise.all([promiseDataDay, promisePlantaRef, promiseUciRef, promiseFallecidosRef]).then(function (v) {
    var percentagePlanta = percentage(v[0].hospitalizados_planta_dia[0], v[1]);
    var percentageUci = percentage(v[0].hospitalizados_uci_dia[0], v[2]);
    var percentageFallecidos = percentage(v[0].fallecidos_dia[0], v[3]);
    if (reference100(percentagePlanta)) document.querySelector('[data-element="hospitalizados_planta_dia"]').parentElement.classList.add(reference100(percentagePlanta), v[0].hospitalizados_planta_dia[1]);
    if (reference100(percentageUci)) document.querySelector('[data-element="hospitalizados_uci_dia"]').parentElement.classList.add(reference100(percentageUci), v[0].hospitalizados_uci_dia[1]);
    if (reference100(percentageFallecidos)) document.querySelector('[data-element="fallecidos_dia"]').parentElement.classList.add(reference100(percentageFallecidos), v[0].fallecidos_dia[1]);
    var averageClass = reference100((percentagePlanta + percentageUci + percentageFallecidos) / 3);
    if (averageClass) document.getElementById('hospitalario_dia').classList.add(averageClass);
  })["catch"](function (resp) {
    return setElement('hospitalario_dia_error', messages.error);
  });
};

var percentage = function percentage(value, reference) {
  return Math.round(value / reference * 100);
};

var reference100 = function reference100(value) {
  if (isNaN(value)) {
    return false;
  }

  if (value > 55) {
    return 'level4';
  }

  if (value > 30) {
    return 'level3';
  }

  if (value > 5) {
    return 'level2';
  }

  return 'level1';
};

var reference100Camas = function reference100Camas(value) {
  if (isNaN(value)) {
    return false;
  }

  if (value > 84) {
    return 'level4';
  }

  if (value >= 69) {
    return 'level2';
  }

  return 'level1';
};

var getOcupacionCamas = function getOcupacionCamas(provincia) {
  var fullUrl = provincia ? "".concat(urls.ocupacion_camas, "&refine.provincia=").concat(provincia) : "".concat(urls.ocupacion_camas);
  var ocupacionCamas = ajax(fullUrl);
  return ocupacionCamas.then(function (resp) {
    var lastIndex = resp.length - 1;
    var tendenciaCamasOcupadasPlanta = calcTendencia(resp, 'camas_ocupadas_planta');
    var tendenciaCamasOcupadasUci = calcTendencia(resp, 'camas_ocupadas_uci');
    setElement('camas_ocupadas_planta', resp[lastIndex].camas_ocupadas_planta);
    setElement('camas_habilitadas_planta', resp[lastIndex].camas_habilitadas_planta);
    setElement('camas_iniciales_planta', resp[lastIndex].camas_iniciales_planta);
    setElement('camas_ocupadas_uci', resp[lastIndex].camas_ocupadas_uci);
    setElement('camas_habilitadas_uci', resp[lastIndex].camas_habilitadas_uci);
    setElement('camas_iniciales_uci', resp[lastIndex].camas_iniciales_uci);
    setDateElement('ocupacion_hospitalaria_fecha', getDateObject(resp[lastIndex].x));
    var percentagePlanta = percentage(resp[lastIndex].camas_ocupadas_planta, resp[lastIndex].camas_habilitadas_planta);
    var percentageUci = percentage(resp[lastIndex].camas_ocupadas_uci, resp[lastIndex].camas_habilitadas_uci);

    if (reference100Camas(percentagePlanta)) {
      var planta = document.querySelector('[data-element="camas_ocupadas_planta"]');
      planta.parentElement.classList.add(reference100Camas(percentagePlanta), tendenciaCamasOcupadasPlanta);
      planta.parentElement.nextElementSibling.nextElementSibling.value = percentagePlanta;
    }

    if (reference100Camas(percentageUci)) {
      var uci = document.querySelector('[data-element="camas_ocupadas_uci"]');
      uci.parentElement.classList.add(reference100Camas(percentageUci), tendenciaCamasOcupadasUci);
      uci.parentElement.nextElementSibling.nextElementSibling.value = percentageUci;
    }

    var averageClass = reference100Camas(percentageUci >= percentagePlanta ? percentageUci : (percentagePlanta + percentageUci) / 2);
    if (averageClass) document.getElementById('ocupacion_hospitalaria').classList.add(averageClass);
  })["catch"](function (resp) {
    setElement('ocupacion_hospitalaria_error', messages.error);
    setElement('camas_ocupadas_planta', '-');
    setElement('camas_habilitadas_planta', '-');
    setElement('camas_ocupadas_uci', '-');
    setElement('camas_habilitadas_uci', '-');
    setDateElement('ocupacion_hospitalaria_fecha');
  });
};

var getNuevosPositivos = function getNuevosPositivos(provincia) {
  var fullUrl = provincia ? "".concat(urls.nuevos_positivos, "&refine.provincia=").concat(provincia) : "".concat(urls.nuevos_positivos);
  var fullUrlNuevosPositivosReference = provincia ? "".concat(urls.nuevos_positivos_reference, "&refine.provincia=").concat(provincia) : "".concat(urls.nuevos_positivos_reference);
  var nuevosPositivos = ajax(fullUrl);
  var nuevosPositivosReference = ajax(fullUrlNuevosPositivosReference);
  var promiseDataDay = nuevosPositivos.then(function (resp) {
    var lastIndex = resp.length - 1;
    var nuevosPositivosValue = resp[lastIndex].nuevos_positivos;
    var tendenciaNuevosPositivos = calcTendencia(resp, 'nuevos_positivos');
    setElement('nuevos_positivos_dia', nuevosPositivosValue);
    setDateElement('nuevos_positivos_fecha', getDateObject(resp[lastIndex].x));
    return [nuevosPositivosValue, tendenciaNuevosPositivos];
  })["catch"](function (resp) {
    setElement('nuevos_positivos_dia', '-');
    setDateElement('nuevos_positivos_fecha');
  });
  var promiseDataRef = nuevosPositivosReference.then(function (resp) {
    var nuevosPositivosReferenceValue = resp[0].nuevos_positivos;
    setElement('nuevos_positivos_reference', nuevosPositivosReferenceValue);
    setDateElement('nuevos_positivos_reference_fecha', getDateObject(resp[0].x));
    return nuevosPositivosReferenceValue;
  })["catch"](function (resp) {
    setElement('nuevos_positivos_reference', '-');
    setDateElement('nuevos_positivos_reference_fecha');
  });
  return Promise.all([promiseDataDay, promiseDataRef]).then(function (v) {
    var percentageDay = percentage(v[0][0], v[1]);
    var levelClass = reference100(percentageDay);

    if (levelClass) {
      document.querySelector('[data-element="nuevos_positivos_dia"]').parentElement.classList.add(levelClass, v[0][1]);
      document.getElementById('nuevos_positivos').classList.add(levelClass);
    }
  })["catch"](function (resp) {
    return setElement('nuevos_positivos_error', messages.error);
  });
};

var getPrevalencia = function getPrevalencia(provincia) {
  var fullUrl = provincia ? "".concat(urls.prevalencia, "&refine.provincia=").concat(provincia) : "".concat(urls.prevalencia);
  var fullUrlPrevalenciaReference = provincia ? "".concat(urls.prevalencia_reference, "&refine.provincia=").concat(provincia) : "".concat(urls.prevalencia_reference);
  var prevalencia = ajax(fullUrl);
  var prevalenciaReference = ajax(fullUrlPrevalenciaReference);
  var promiseDataDay = prevalencia.then(function (resp) {
    var lastIndex = resp.length - 1;
    var prevalenciaValue = resp[lastIndex].prevalencia;
    var tendenciaPrevalencia = calcTendencia(resp, 'prevalencia');
    setElement('prevalencia_dia', prevalenciaValue);
    setDateElement('prevalencia_fecha', getDateObject(resp[lastIndex].x));
    return [prevalenciaValue, tendenciaPrevalencia];
  })["catch"](function (resp) {
    setElement('prevalencia_dia', '-');
    setDateElement('prevalencia_fecha');
  });
  var promiseDataRef = prevalenciaReference.then(function (resp) {
    var prevalenciaReferenceValue = resp[0].prevalencia;
    setElement('prevalencia_reference', prevalenciaReferenceValue);
    setDateElement('prevalencia_reference_fecha', getDateObject(resp[0].x));
    return prevalenciaReferenceValue;
  })["catch"](function (resp) {
    setElement('prevalencia_reference', '-');
    setDateElement('prevalencia_reference_fecha');
  });
  return Promise.all([promiseDataDay, promiseDataRef]).then(function (v) {
    var percentageDay = percentage(v[0][0], v[1]);
    var levelClass = reference100(percentageDay);

    if (levelClass) {
      document.querySelector('[data-element="prevalencia_dia"]').parentElement.classList.add(levelClass, v[0][1]);
      document.getElementById('prevalencia').classList.add(levelClass);
    }
  })["catch"](function (resp) {
    return setElement('prevalencia_error', messages.error);
  });
};

var init = function init() {
  var provinciaParam = stripTags(getParam('provincia'));
  var provinciaElements = document.querySelectorAll('[data-element="provincia"]');
  var provincia = provincias.includes(provinciaParam) ? provinciaParam : 'Castilla y León';
  var selects = document.querySelectorAll('select');

  for (var key in selects) {
    if (selects.hasOwnProperty(key)) {
      selects[key].value = provincia;
    }
  }

  setElement('title', "Situaci\xF3n simplificada COVID19 en ".concat(provincia));

  for (var _key in provinciaElements) {
    if (provinciaElements.hasOwnProperty(_key)) {
      provinciaElements[_key].innerText = provincia;
    }
  }

  provincia = provincia === 'Castilla y León' ? false : encodeURIComponent(provincia);
  Promise.all([getRastreadores(provincia), getPositivos14(provincia), getHospitalarioDia(provincia), getOcupacionCamas(provincia), getNuevosPositivos(provincia), getPrevalencia(provincia), getIndicadoresDeRiesgo(provincia)]).then(function () {
    return orderSections();
  });
};

document.addEventListener('DOMContentLoaded', init);

if ('serviceWorker' in navigator) {
  window.addEventListener('load', function () {
    navigator.serviceWorker.register('/service-worker.js');
  });
}

/***/ }),

/***/ "./resources/sass/main.scss":
/*!**********************************!*\
  !*** ./resources/sass/main.scss ***!
  \**********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!***************************************************************!*\
  !*** multi ./resources/js/main.js ./resources/sass/main.scss ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /home/osc/Proyectos/cyl19/cyl19/resources/js/main.js */"./resources/js/main.js");
module.exports = __webpack_require__(/*! /home/osc/Proyectos/cyl19/cyl19/resources/sass/main.scss */"./resources/sass/main.scss");


/***/ })

/******/ });