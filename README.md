[![Netlify Status](https://api.netlify.com/api/v1/badges/eadec959-ae6b-4f64-ac6b-e9a1fedc6128/deploy-status)](https://app.netlify.com/sites/cyl19/deploys)

# Situación simplificada COVID19 en Castilla y León

Consulta la situación actualizada de la epidemia por coronavirus en Casilla y León de manera simple y accesible en [cyl19.ogarcia.es](https://cyl19.ogarcia.es).

Realizado con [Skeleventy](https://skeleventy.netlify.com/) como punto de partida y utilizando los [datos abiertos de la Junta de Castilla y León](https://datosabiertos.jcyl.es/web/es/datos-abiertos-castilla-leon.html).

Este proyecto trata de dar acceso a los datos de la epidemia COVID19 a todas aquellas personas que por motivos de accesibilidad les es difícil, e incluso imposible, leer y/o interpretar los [gráficos y tablas de la Junta](https://analisis.datosabiertos.jcyl.es/pages/coronavirus/). Cualquier sugerencia en general, y en este aspecto en particular, es más que bienvenida.